#pragma once

#include <other/BaseMod.h>
#include <utility/Connection.h>
#include <utility/Dispatcher.h>
#include <utility/Logger.h>
#include <other/CommandParser.h>
#include <other/CommandSystem.h>

class SatisAnarchy : SML::BaseMod {
public:
	SML::Globals* globalsReference;

	const char* Name() { 
		return "SatisAnarchy"; 
	};
	const char* Version() { 
		return "0.2"; 
	};
	const char* Description() { 
		return "Remove building restrictions."; 
	};
	const char* Authors() { 
		return "Florens"; 
	};
	virtual const std::vector<const char*> Dependencies() {
		return std::vector<const char*>{
			// list here
		};
	}

	void PreSetup(SML::Globals* globals) {
		SML::mod_info(Name(), "Setting up ", Name());
		globalsReference = globals;

		Setup();
	}

	void Cleanup();

protected:
	void Setup();
};

GLOBAL SatisAnarchy* CreateMod() {
	return new SatisAnarchy;
}