#include <stdafx.h>
#include <utility/Configuration.h>
#include <other/CommandSystem.h>
#include <events/PlayerEvents.h>
#include <other/CommandSystem.h>
#include <other/CommandParser.h>
#include "SatisAnarchy.h"
#include "ModReturns.h"
#include "Internals.h"

using event = SML::HookLoader::Event;

// if you want to access the Global values
SatisAnarchy* mod;
SML::Configuration config("SatisAnarchy");

bool enabled = true;
char toggleKey;

void ToggleKey(SML::ModReturns* returns, void* input, void* key, void* event, float amountDepressed, bool gamepad) {
	if (GetAsyncKeyState(toggleKey)) {
		SML::mod_info(mod->Name(), "Toggled anarchy");
		enabled = !enabled;
	}
}

void OverrideCanConstruct(SML::ModReturns* returns, void* hologram) {
	// SML::Hologram* tmpHologram = new SML::Hologram(hologram);

	/*
	void* displayName = tmpHologram->recipe->_class->displayName;

	// Convert to string
	auto pointer = satisAnarchy->GlobalsReference->functions[event::FTextToStringGlobal];
	std::string str = ((std::string(__stdcall*)(void*))pointer)(displayName);

	SML::mod_info(satisAnarchy->Name(), str);
	*/

	// SML::mod_info(satisAnarchy->Name(), std::to_string((int)tmpHologram->scrollRotation));

	if (enabled) {
		bool ret = true;
		returns->ReturnValue = &ret;
		returns->UseOriginalFunction = false;
	}
}

void OverrideIsValidHitActor(SML::ModReturns* returns, void* hologram, void* actor) {
	if (enabled) {
		bool ret = true;
		returns->ReturnValue = &ret;
		returns->UseOriginalFunction = false;
	}
}

void SatisAnarchy::Setup() {
	mod = this;

	if (!config.exists()) {
		config.set("ToggleKey", "G");
		config.save();
	}

	config.load();
	toggleKey = config.get<std::string>("ToggleKey", "G")[0];
	SML::mod_info(mod->Name(), "Toggle Key: ", toggleKey);

	_dispatcher.subscribe(SML::HookLoader::Event::AFGHologramCanConstruct, OverrideCanConstruct);
	_dispatcher.subscribe(SML::HookLoader::Event::AFGHologramIsValidHitActor, OverrideIsValidHitActor);
	_dispatcher.subscribe(SML::HookLoader::Event::UPlayerInputInputKey, ToggleKey);
}

void SatisAnarchy::Cleanup() {

}